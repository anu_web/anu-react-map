import React, { Component } from 'react'
import { withScriptjs, withGoogleMap, GoogleMap } from "react-google-maps"

const MapWrapper = withScriptjs( withGoogleMap( props => (
  <GoogleMap 
    defaultZoom={ props.config.zoom }
    defaultCenter={ props.config.center }>
    { props.children }
  </GoogleMap>
)))

export default MapWrapper


// import React, { Component } from 'react'
// import style from './map.css'

// class MapView extends Component {
//   constructor(props) {
//     super(props)
//     this.map = React.createRef()
//   }

//   componentDidMount() {
//     this.renderMap((map) => {
//       this.fetchChildren(map)
//     })
//   }

//   renderMap(readyCallback) {
//     const { config, listeners } = this.props
//     let map = new window.google.maps.Map( this.map.current, config )

//     window.google.maps.event.addListenerOnce(map, 'idle', () => {
//       readyCallback(map)
//     })

//     this.addListeners(map, listeners)
//   }

//   renderMarker(map, component) {
//     const { listeners, marker } = component.props
//     let _marker = new window.google.maps.Marker({
//       title: marker.title,
//       position: new window.google.maps.LatLng(
//         marker.point
//       ),
//       icon: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
//     })

//     _marker.setMap(map)
//     _marker._meta = marker

//     this.addListeners(_marker, listeners)    
//   }

//   renderPolygon(map, component) {
//     const { listeners, polygon } = component.props
//     let _polygon = new window.google.maps.Polygon({
//       paths: polygon,
//       strokeColor: '#FF0000',
//       strokeOpacity: 0.8,
//       strokeWeight: 2,
//       fillColor: '#FF0000',
//       fillOpacity: 0.35,
//     })

//     _polygon.setMap(map)
//     this.addListeners(_polygon, listeners)
//   }

//   renderPolyline(map, component) {
//     const { listeners, polyline } = component.props
//     let _polyline = new window.google.maps.Polyline({
//       path: polyline,
//       strokeColor: '#FF0000',
//       strokeOpacity: 1.0,
//       strokeWeight: 6,
//     })

//     _polyline.setMap(map)
//     this.addListeners(_polyline, listeners)
//   }

//   addListeners(component, listeners) {
//     if ( !!listeners ) {
//       Object.keys(listeners).forEach((key) => {

//         if (key === 'ready') {
//           return listeners[key]( component )
//         }

//         return component.addListener(key, () => { listeners[key]( component ) })
//       })
//     }     
//   }

//   fetchChildren(map) {
//     const { children } = this.props

//     this.getRecursiveChildren( children, (component) => {
//       switch(component.type.name) {
//         case 'Marker':
//           this.renderMarker(map, component)
//           break

//         case 'Polygon':
//           this.renderPolygon(map, component)
//           break

//         case 'Polyline':
//           this.renderPolyline(map, component)
//           break

//         default: 
//           break
//       }
//     })
//   }

//   getRecursiveChildren(children, fn) {
//     return React.Children.map( children, child => {
//       if (!React.isValidElement(child)) {
//         return child
//       }

//       if (child.props.children) {
//         child = React.cloneElement(child, {
//           children: this.getRecursiveChildren(child.props.children, fn)
//         })
//       }

//       return fn(child)
//     })
//   }

//   render() {    
//     return (
//       <div ref={ this.map } className={ style.map }></div>
//     )
//   }
// }

// export default MapView