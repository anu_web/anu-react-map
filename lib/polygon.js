import React, { PureComponent } from 'react'
import { Polygon, InfoWindow } from 'react-google-maps'

class PolygonWrapper extends PureComponent {
	constructor(props) {
		super(props)
		this.state = { 
			callout: false,
			position: false, 
		}

		this.ref = React.createRef()
		this.showCallout = this.showCallout.bind(this)
		this.closeCallout = this.closeCallout.bind(this)

	}

	showCallout(event) {
		this.setState({ callout: true, position: event.latLng })
		this.props.onClick(event)
	}

	closeCallout(event) {
		this.setState({ callout: false })
	}

  render() {
  	const { children, options, path } = this.props
  	const { callout, position } = this.state

    return (
    	<div>
	    	<Polygon options={ options } path={ path } onClick={ this.showCallout } />
	    	{ !! callout && !! position &&
	    		<InfoWindow position={ position } onCloseClick={ this.closeCallout }>
	    			{ children }
	    		</InfoWindow> 
	    	}
	    </div>
    )
  }
}

export default PolygonWrapper