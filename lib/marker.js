import React, { PureComponent } from 'react'
import { Marker, InfoWindow } from 'react-google-maps'

class MarkerWrapper extends PureComponent {
	constructor(props) {
		super(props)
		this.state = { callout: false }

		this.ref = React.createRef()
		this.showCallout = this.showCallout.bind(this)
		this.closeCallout = this.closeCallout.bind(this)
	}

	showCallout(event) {
		this.setState({ callout: true })
		this.props.onClick(event)
	}

	closeCallout(event) {
		this.setState({ callout: false })
	}

  render() {
  	const { children, icon, position } = this.props
  	const { callout } = this.state

    return (
	    <Marker icon={ icon } position={ position } onClick={ this.showCallout }>
	    	{ !! callout && 
	    		<InfoWindow onCloseClick={ this.closeCallout }>
	    			{ children }
	    		</InfoWindow>
	    	}
	    </Marker>
    )
  }
}

export default MarkerWrapper