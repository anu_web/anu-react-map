import { default as MapView } from './lib/map'
import { default as MarkerWrapper } from './lib/marker'
import { default as PolylineWrapper } from './lib/polyline'
import { default as PolygonWrapper } from './lib/polygon'

MapView.Marker = MarkerWrapper
MapView.Polyline = PolylineWrapper
MapView.Polygon = PolygonWrapper

export { MapView }
